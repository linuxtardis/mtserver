#include <iostream>
#include <list>
#include <experimental/filesystem>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <openssl/sha.h>

namespace fs = std::experimental::filesystem;
using filelist = std::list<fs::path>;

void search(filelist &out, const fs::path &dir) {
    for (auto &p : fs::recursive_directory_iterator(dir, fs::directory_options::follow_directory_symlink)) {
        auto &&path = fs::canonical(p.path());

        if (!fs::is_symlink(path) && !fs::is_regular_file(path)) {
            continue;
        }

        auto &&ext = path.extension().string();
        std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);
        if (ext == ".png" || ext == ".ogg" || ext == ".x" || ext == ".blend" || ext == ".xcf") {
            out.push_back(path);
        }
    }
}

fs::path parseDirPath(const char *value) {
    fs::path dir = fs::u8path(value);
    if (!fs::is_directory(dir)) {
        std::cerr << "directory '" << dir << "' does not exist." << std::endl;
        throw std::exception();
    }
    return fs::canonical(dir);
}

std::ofstream initMths(const fs::path &cachedir) {
    fs::create_directories(cachedir);
    fs::path mths = cachedir / "index.mth";
    std::ofstream stream(mths, std::ios::out | std::ios::trunc | std::ios::binary);
    stream.write("MTHS\x00\x01", 6);
    return stream;
}

long readBytes(const fs::path &path, std::vector<unsigned char> &cache) {
    std::ifstream file(path);
    file.seekg(0, std::ios::end);
    auto len = static_cast<size_t>(file.tellg());
    file.seekg(0, std::ios::beg);

    while (cache.size() < len) {
        cache.assign(cache.size() * 2, 0);
    }

    file.read(reinterpret_cast<char *>(cache.data()), len);
    file.close();

    return len;
}

std::string sha1str(unsigned char *hash) {
    std::ostringstream hexstr;
    hexstr << std::hex << std::setfill('0') << std::nouppercase;
    for (int i = 0; i < SHA_DIGEST_LENGTH; i++) {
        hexstr << std::setw(2) << (int) hash[i];
    }
    return hexstr.str();
}

void doHash(const fs::path &cachedir,
            const fs::path &path,
            std::ofstream &ofstream,
            std::vector<unsigned char> &cache) {
    auto len = static_cast<size_t>(readBytes(path, cache));
    unsigned char hash[SHA_DIGEST_LENGTH];
    SHA1(cache.data(), len, hash);
    ofstream.write(reinterpret_cast<char *>(hash), SHA_DIGEST_LENGTH);
    fs::path symlink = cachedir / sha1str(hash);
    fs::create_symlink(path, symlink);
}

int main(int argc, char **argv) {
    fs::path self = fs::u8path(argv[0]);

    if (argc < 3) {
        std::cerr << "Usage: " << std::endl;
        std::cerr << self.filename() << " <cache directory> <moddir1>..." << std::endl;
        return 1;
    }

    fs::path cachedir = parseDirPath(argv[1]);

    std::list<fs::path> moddirs;

    for (int i = 2; i < argc; i++) {
        moddirs.push_back(parseDirPath(argv[i]));
    }

    filelist images;
    for (auto &&moddir : moddirs) {
        search(images, moddir);
    }

    std::vector<unsigned char> cache(1024 * 1024);
    auto &&stream = initMths(cachedir);
    for (auto &&resource : images) {
        doHash(cachedir, resource, stream, cache);
    }
    stream.close();

    return 0;
}
