#!/bin/bash
if [ "$EUID" -ne "0" ]; then
  echo "Root required"
  exit 1
fi

cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")"

rm -rf ./contents
mkdir ./contents
./minecacher $(pwd)/contents $(pwd)/../mods $(pwd)/../games
chown www-data:www-data -R ./contents
